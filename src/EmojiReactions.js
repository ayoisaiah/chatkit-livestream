import React from "react";
import Twemoji from 'react-twemoji';

const EmojiReactions = id => {
  return (
    <div className="emoji-reactions">
      <div className="reactions">
        <Twemoji options={{ className: "twemoji" }}>
          <span role="img" aria-label="😍">
            😍
          </span>
        </Twemoji>
        <Twemoji options={{ className: "twemoji" }}>
          <span role="img" aria-label="😆">
            😆
          </span>
        </Twemoji>
        <Twemoji options={{ className: "twemoji" }}>
          <span role="img" aria-label="😲">
            😲
          </span>
        </Twemoji>
        <Twemoji options={{ className: "twemoji" }}>
          <span role="img" aria-label="😢">
            😢
          </span>
        </Twemoji>
        <Twemoji options={{ className: "twemoji" }}>
          <span role="img" aria-label="😠">
            😠
          </span>
        </Twemoji>
        <Twemoji options={{ className: "twemoji" }}>
          <span role="img" aria-label="👍">
            👍
          </span>
        </Twemoji>
        <Twemoji options={{ className: "twemoji" }}>
          <span role="img" aria-label="👎">
            👎
          </span>
        </Twemoji>
      </div>
    </div>
  );
};

export default EmojiReactions;
