import Message from "./Message";

const ChatSession = props => {
  const { messages, currentUser, } = props;
  const arr = messages.map(message => {
    return Message(message, currentUser);
  });

  return arr.flat();
};

export default ChatSession;
