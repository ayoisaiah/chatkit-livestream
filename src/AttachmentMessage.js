import React from "react";
import ImageZoom from 'react-medium-image-zoom';

const AttachmentMessage = message => {
  return (
    <div className="media">
    <ImageZoom
      image={{
        src: message.src,
        alt: '',
        className: 'image-attachment',
      }}
      zoomImage={{
        src: message.src,
        className: 'image-attachment--zoom',
        alt: '',
      }}
      defaultStyles={{
        overlay: { backgroundColor: "rgba(0,0,0,0.3)" }
      }}
    />
  </div>
  );
};

export default AttachmentMessage;
