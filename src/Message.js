import React from "react";
import Avatar from "react-avatar";
import TextMessage from "./TextMessage";
import AttachmentMessage from "./AttachmentMessage";
import Timeago from 'react-timeago';

const Message = (message, currentUser) => {
  const user = message.senderId === currentUser.id ? "user" : "";
  const time = <Timeago date={message.updatedAt} />;
  const color = user ? "rebeccapurple" : "";

  const arr = message.parts.map((p, i) => {
    if (p.partType === "inline") {
      const m = {
        id: message.id,
        index: i,
        updatedAt: message.updatedAt,
        senderId: message.senderId,
        text: p.payload.content
      };

      return TextMessage(m, currentUser);
    }

    if (p.partType === "url") {
      const m = {
        id: message.id,
        index: i,
        updatedAt: message.updatedAt,
        senderId: message.senderId,
        src: p.payload.url
      };

      return AttachmentMessage(m, currentUser);
    }

    if (p.partType === "attachment") {
      const m = {
        id: message.id,
        index: i,
        updatedAt: message.updatedAt,
        senderId: message.senderId,
        src: p.payload._downloadURL
      };

      if (Date.now() > Date.parse(p.payload._expiration)) {
        p.payload._fetchNewDownloadURL();
      }

      return AttachmentMessage(m, currentUser);
    }

    return null;
  });

  return (
    <li key={message.id} className={user}>
      <Avatar
        color={color}
        name={message.senderId}
        size={"36px"}
        round={true}
      />
      <div className="content">
        {arr}

        <span className="time">{time}</span>
      </div>
    </li>
  );
};

export default Message;
