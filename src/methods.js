import { ChatManager, TokenProvider } from "@pusher/chatkit-client";

function handleKeyPress(event) {
  if (event.key === "Enter") {
    event.preventDefault();
    this.sendMessage();
  }
}

function handleInput(event) {
  const { value, name } = event.target;

  this.setState({
    [name]: value
  });
}

function toggleEmojiPicker() {
  this.setState({
    showEmojiPicker: !this.state.showEmojiPicker,
    showGiphyPicker: false
  });
}

function toggleGiphyPicker() {
  this.setState({
    showGiphyPicker: !this.state.showGiphyPicker,
    showEmojiPicker: false
  });
}

function toggleImagePicker() {
  this.setState({
    showImagePicker: !this.state.showImagePicker
  });
}

function addEmoji(emoji) {
  const { newMessage } = this.state;
  const text = `${newMessage}${emoji.native}`;

  this.setState({
    newMessage: text,
    showEmojiPicker: false
  });
}

function connectToChatkit() {
  const { userId } = this.state;

  const tokenProvider = new TokenProvider({
    url:
      "https://us1.pusherplatform.io/services/chatkit_token_provider/v1/0c668823-ec59-4ebd-a1cf-a5932d8d7070/token"
  });

  const chatManager = new ChatManager({
    instanceLocator: "v1:us1:0c668823-ec59-4ebd-a1cf-a5932d8d7070",
    userId,
    tokenProvider
  });

  return chatManager
    .connect()
    .then(currentUser => {
      this.setState(
        {
          currentUser,
          rooms: currentUser.rooms
        },
        () => connectToRoom.call(this)
      );
    })
    .catch(console.error);
}

function connectToRoom() {
  const { currentUser } = this.state;
  const roomId = "21207962"
  this.setState({
    messages: []
  });

  return currentUser
    .subscribeToRoomMultipart({
      roomId,
      messageLimit: 100,
      hooks: {
        onMessage: message => {
          const m = this.state.messages;
          this.setState({
            messages: [...m, message],
          });
        },
        onPresenceChanged: () => {
          const { currentRoom } = this.state;
          this.setState({
            roomUsers: currentRoom.users.sort(a => {
              if (a.presence.state === "online") return -1;

              return 1;
            })
          });
        }
      }
    })
    .then(currentRoom => {
      this.setState({
        currentRoom,
        rooms: currentUser.rooms,
      });
    })
    .catch(console.error);
}

function sendMessage() {
  const { newMessage, currentUser, currentRoom, pictures } = this.state;
  const parts = [];

  if (newMessage.trim() === "" && pictures.length === 0) return;

  if (newMessage.trim() !== "") {
    parts.push({
      type: "text/plain",
      content: newMessage
    });
  }

  pictures.forEach(pic => {
    parts.push({
      file: pic
    });
  });

  currentUser.sendMultipartMessage({
    roomId: `${currentRoom.id}`,
    parts
  });

  this.setState({
    newMessage: "",
    pictures: [],
    showImagePicker: false
  });
}

function onDrop(pictureFiles, pictureDataURLs) {
  this.setState({
    pictures: this.state.pictures.concat(pictureFiles)
  });
}

function sendGif(gif) {
  const { currentRoom, currentUser } = this.state;

  currentUser.sendMultipartMessage({
    roomId: `${currentRoom.id}`,
    parts: [
      {
        type: "image",
        url: gif.images.original.gif_url
      }
    ]
  });

  this.setState({
    showGiphyPicker: false
  });
}

export {
  handleInput,
  toggleEmojiPicker,
  toggleImagePicker,
  addEmoji,
  connectToChatkit,
  sendMessage,
  handleKeyPress,
  onDrop,
  toggleGiphyPicker,
  sendGif,
};
