import React from "react";
import MicrolinkCard from "@microlink/react";

const insertTextAtIndices = (text, obj) => {
  return text.replace(/./g, function(character, index) {
    return obj[index] ? obj[index] + character : character;
  });
};

const TextMessage = message => {
  const urlMatches = message.text.match(/\b(http|https)?:\/\/\S+/gi) || [];
  const LinkPreviews = urlMatches.map((link, i) => (
    <MicrolinkCard key={i} url={link} size="large" />
  ));

  let { text } = message;
  urlMatches.forEach(link => {
    const startIndex = text.indexOf(link);
    const endIndex = startIndex + link.length;
    text = insertTextAtIndices(text, {
      [startIndex]: `<a href="${link}" target="_blank" rel="noopener noreferrer" class="embedded-link">`,
      [endIndex]: "</a>"
    });
  });

  return (
    <div>
      <div className="message">
        <div className="bubble">
          <p
            dangerouslySetInnerHTML={{
              __html: text
            }}
          />
        </div>
      </div>

      {LinkPreviews}
    </div>
  );
};

export default TextMessage;
